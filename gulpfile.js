'use strict';
const gulp = require('gulp');
const g = require('gulp-load-plugins')();
// const protractor = g.protractor.protractor;
const stylish = require('jshint-stylish'); // jshint ignore:line
const browserSync = require('browser-sync');
const reload = browserSync.reload;
const spawn = require('child_process').spawn;
const bowerFiles = require('main-bower-files');
const del = require('del');
const cachebuster = new g.cachebust();
const open = require('open');
const _ = require('lodash');

let paths = {
  html: 'src/**/*.html',
  sass: 'src/style/**/*.scss',
  dist: 'dist/',
  js: [
    'src/scripts/*.js',
    // 'src/app/*.js',
    // 'src/app/**/*.js',
    // '!src/app/*.test.js',
    // '!src/app/**/*.test.js'
    ],
  jsTest: ['src/app/auth.test.js', 'src/app/*.test.js', 'src/app/**/*.test.js'],
  jsLib: bowerFiles('**/*.js'),
  cssLib: bowerFiles('**/*.css'),
  jsProdDist: ['dist/lib/lib*.js', 'dist/app/app*.js'],
  jsLibDist: 'dist/lib',
  cssProdDist: 'dist/style/main*.css',
  cssProdLib: 'dist/lib/lib*.css',
  cssDist: 'dist/style/',
  mocksDist: 'dist/mocks/'
};
// console.log(paths.cssLib);

const prodBuild = g.util.env.prod || false;

if(prodBuild) {
  console.log('//// PROD BUILD ////');
} else {
  console.log('//// DEV BUILD ////');
}

if(prodBuild) {
  paths.js.push('!src/app/configDev.js');
} else {
  paths.js.push('!src/app/configProd.js');
}

gulp.task('clean', function(cb) {
  del([
    'dist'
  ], cb);
});

// scripts
gulp.task('jsLib', function() {
  return gulp.src(paths.jsLib, {base: 'src/lib'})
    .pipe(g.if(prodBuild, g.concat('lib.js')))
    .pipe(g.if(prodBuild, g.uglify()))
    .pipe(g.if(prodBuild, cachebuster.resources()))
    .pipe(gulp.dest(paths.dist + 'lib/'));
});
gulp.task('cssLib', function() {
  return gulp.src(paths.cssLib, {base: 'src/lib'})
    .pipe(g.if(prodBuild, g.concat('lib.css')))
    // .pipe(g.if(prodBuild, g.replace(new RegExp('../fonts/(\\w)*/?', 'g'), '/lib/fonts/')))
    .pipe(g.if(prodBuild, g.cleanCss()))
    .pipe(g.if(prodBuild, cachebuster.resources()))
    .pipe(gulp.dest(paths.dist + 'lib/'));
});
gulp.task('fonts', function() {
  return gulp.src('src/lib/**/fonts/**/*')
    .pipe(g.flatten())
    .pipe(g.if(prodBuild,gulp.dest(paths.dist + 'lib/fonts')))
    .pipe(g.if(!prodBuild,gulp.dest(paths.dist + 'fonts/')));
});
gulp.task('img', function() {
  return gulp.src('src/img/**')
    .pipe(g.if(prodBuild, cachebuster.resources()))
    .pipe(gulp.dest(paths.dist + 'img/'));
});

gulp.task('lib', ['jsLib', 'cssLib', 'fonts', 'img'], function() {
  return;
});

// hinting
gulp.task('hint', function() {
  gulp.src(paths.js)
    .pipe(g.eslint())
      .pipe(g.eslint.format('stylish'));
});

// testing
gulp.task('webdriver-update', g.protractor.webdriver_update); // jshint ignore:line
// gulp.task('webdriver-standalone', g.protractor.webdriver_standalone);
gulp.task('jstest', ['webdriver-update'], function() {
  return gulp.src(paths.jsTest)
    .pipe(g.protractor.protractor({
      configFile: 'protractor.conf.js',
      args: ['--baseUrl', 'http://localhost:4000']
    }));
});

// open this manually since browser sync won't do it
gulp.task('open', ['build'], function() {
  open('http://localhost:4000');
});

// process js files
gulp.task('js', function() {
  return gulp.src(paths.js)
    .pipe(g.sourcemaps.init())
    .pipe(g.babel({
      presets: ['es2015']
    }))
    .pipe(g.if(prodBuild, g.concat('app.js')))
    .pipe(g.if(prodBuild, g.uglify().on('error', g.util.log)))

    .pipe(g.sourcemaps.write())
    .pipe(g.if(prodBuild, cachebuster.resources()))
    .pipe(gulp.dest(paths.dist + 'scripts'))
    .pipe(reload({stream: true}));
});


// styles
gulp.task('css', ['lib'], function() {
  return gulp.src(paths.sass)
    .pipe(g.sourcemaps.init())
      .pipe(g.sass().on('error', g.sass.logError))
      .pipe(g.autoprefixer())
      .pipe(g.cleanCss())
    .pipe(g.sourcemaps.write())
    .pipe(g.if(prodBuild, cachebuster.references()))
    .pipe(g.if(prodBuild, cachebuster.resources()))
    .pipe(gulp.dest(paths.cssDist))
    .pipe(reload({stream: true}));
});

// markup
gulp.task('html', ['js', 'lib', 'css'], function() {
  var sources = paths.jsLib.concat(paths.js).concat(paths.cssLib).concat(paths.cssProdDist);
  //jquery should be loaded first
  var jqueryElement = _.find(sources, function (el) { return el.indexOf('jquery') > -1; });
  sources = _(sources).without(jqueryElement).unshift(jqueryElement).value();
  gulp.src(paths.html)
    .pipe(g.if(!prodBuild, g.inject(
      gulp.src(sources, {read: false}),
      {relative: true, ignorePath: '../dist'}
    )))
    .pipe(g.if(prodBuild, g.inject(
      gulp.src(paths.jsProdDist.concat(paths.cssProdLib).concat(paths.cssProdDist), {read: false}),
      {relative: true, ignorePath: '../dist'}
    )))
    .pipe(gulp.dest(paths.dist))
    .pipe(reload({stream: true}));
});

gulp.task('build', ['html']);

gulp.task('serve', function() {
  browserSync({
    server: {
      baseDir: 'dist/'
    },
    ghostMode: false,
    port: 4000,
    open: false
  });
});

gulp.task('watch', function() {
  gulp.watch(paths.html, ['html']);
  gulp.watch(paths.js, ['js']);
  gulp.watch(paths.sass, ['css']);
});

gulp.task('autoreload', function() {
  var p;
  function spawnChildren() {
    if(p) {
      p.kill();
    }
    p = spawn('gulp', ['build', 'watch'], {stdio: 'inherit'});
  }
  gulp.watch('gulpfile.js', spawnChildren);
});

gulp.task('default', ['open', 'watch', 'serve']);
