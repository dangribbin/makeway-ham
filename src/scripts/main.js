function closeMobileNav () {
  $('nav.mobile').hide();
  $('body, html').removeClass('noscroll');
}

function openMobileNav () {
  $('body, html').addClass('noscroll');
  $('nav.mobile').show();
}


$('.mobile-nav-close-button').on('click', closeMobileNav)
$('.mobile-nav-open-button').on('click', openMobileNav)

